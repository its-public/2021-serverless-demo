from website.app import create_app


app = create_app({
    'SECRET_KEY': 'zFU5bkMNBkqmwC',
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'SQLALCHEMY_DATABASE_URI': 'sqlite:///persons.db',
})


@app.cli.command()
def initdb():
    from website.models import db
    db.create_all()
