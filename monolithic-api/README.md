# Sample Persons API implemented with Flask/Python

## How to run application locally

1. Install requirements

   ```shell
   pip install -r requirements.txt
   ```
   
1. Initialize database

   ```shell
   flask initdb
   ```
   
1. Run application
   
   ```shell
   flask run
   ```

API will be available at http://localhost:5000/

