from flask.json import JSONEncoder

from website.models import Person


class CustomJsonEncoder(JSONEncoder):
    """
    Custom JSON encoder which helps to turn
    our models into JSON for serialization
    """

    def default(self, obj):
        if isinstance(obj, Person):
            return obj.to_json()

        return JSONEncoder.default(self, obj)