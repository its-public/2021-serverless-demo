import re
from typing import Dict

CASE_CONVERT_REGEX = re.compile(r'(?<!^)(?=[A-Z])')


def _camel_case_to_snake_case(val: str) -> str:
    """
    Converts string in camel case format to snake case.
    The former is usual for JSON whereas the latter is
    as per Python standards
    :param val: String value in camel case
    :return: String value in snake case
    """
    return CASE_CONVERT_REGEX.sub('_', val).lower()


def _snake_case_to_camel_case(val: str) -> str:
    """
    Converts string in snake case format to camel case.
    The former is a standard in Python whereas the latter is
    used in JSON, for example

    :param val: String value in snake case
    :return: String value in camel case
    """
    components = val.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])


def _map_to_sql_alchemy(val: Dict) -> Dict:
    """
    Converts dictionary keys from camel case to snake case.
    :param val: Dictionary with keys in camel case,
                e.g. dict created from JSON
    :return: Dictionary with keys in snake case
    """
    return {_camel_case_to_snake_case(k): v for k, v in val.items()}


def _map_to_json(val: Dict) -> Dict:
    """
    Converts dictionary keys from snake case to camel case.

    :param val: Dictionary with keys in snake case
    :return:  Dictionary with keys in camel case
    """
    return {_snake_case_to_camel_case(k): v for k, v in val.items()}
