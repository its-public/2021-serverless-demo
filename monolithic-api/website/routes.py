from flask import Blueprint, request
from flask import jsonify

from .models import db, Person

bp = Blueprint('home', __name__)


@bp.route('/persons', methods=['GET', 'POST'])
def person_list():
    if request.method == 'POST':
        person = Person(**request.json)
        db.session.add(person)
        db.session.commit()

        return jsonify(person.to_json())
    elif request.method == 'GET':
        return jsonify(persons=Person.query.order_by(Person.person_id).all())


@bp.route('/persons/<int:person_id>', methods=['GET', 'DELETE'])
def person(person_id: int):
    if request.method == 'GET':
        person = Person.query.get(person_id)
        if person is None:
            return {'error': {'message': f'Person with ID {person_id} not found'}}, 404

        return jsonify(person)
    elif request.method == 'DELETE':
        rows = Person.query.filter(Person.person_id == person_id).delete()
        if rows == 0:
            return {'error': {'message': f'Person with ID {person_id} not found'}}, 404
        db.session.commit()

        return 'OK', 200
