from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

ISO_DATE_FORMAT = '%Y-%m-%d'

db = SQLAlchemy()


class Person(db.Model):
    """
    Person database model.

    It uses straightforward mapping between camel case properties
    as they appear in API/JSON and snake case properties as they
    appear in database. For a more complex cases it's better to define
    some generic mapping mechanims for this. E.g. using utility methods
    _map_to_sql_alchemy() and _map_to_json() define in utils.py
    """
    person_id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(40), nullable=False)
    last_name = db.Column(db.String(40), nullable=False)
    date_of_birth = db.Column(db.Date(), nullable=False)
    custom_fields = db.Column(db.String(2048), nullable=True)

    def __init__(self, **kwargs):
        self.person_id = kwargs.get('personId')
        self.first_name = kwargs.get('firstName')
        self.last_name = kwargs.get('lastName')
        self.date_of_birth = datetime.strptime(kwargs.get('dateOfBirth'), ISO_DATE_FORMAT)

        custom_fields = kwargs.get('customFields', None)
        if custom_fields is not None:
            self.custom_fields = ';'.join([f'{i["key"]}={i["value"]}' for i in custom_fields])

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    def to_json(self):
        custom_fields = None

        if self.custom_fields is not None:
            custom_fields = []
            for item in self.custom_fields.split(';'):
                values = item.split('=')
                custom_fields.append({'key': values[0], 'value': values[1]})

        return {
            'personId': self.person_id,
            'firstName': self.first_name,
            'lastName': self.last_name,
            'dateOfBirth': datetime.strftime(self.date_of_birth, ISO_DATE_FORMAT),
            'customFields': custom_fields
        }
