import os
from flask import Flask
from flask_cors import CORS

from .custom_json import CustomJsonEncoder
from .models import db
from .routes import bp


def create_app(config=None):
    app = Flask(__name__)
    CORS(app)

    # Load default configuration
    app.config.from_object('website.settings')

    # Load environment configuration
    if 'WEBSITE_CONF' in os.environ:
        app.config.from_envvar('WEBSITE_CONF')

    # Load app specified configuration
    if config is not None:
        if isinstance(config, dict):
            app.config.update(config)
        elif config.endswith('.py'):
            app.config.from_pyfile(config)

    setup_app(app)
    app.json_encoder = CustomJsonEncoder
    return app


def setup_app(app):
    db.init_app(app)
    app.register_blueprint(bp, url_prefix='')
