import { Switch, Route, Redirect } from 'react-router-dom';
import { PersonsTable } from './persons-table';
import { PersonDetails } from './person-details';
import { PersonCreateForm } from './person-create-form';

export const PagesRouting = () => {
  return (
    <Switch>
      <Route exact path="/" component={PersonsTable} />
      <Route exact path="/new" component={PersonCreateForm} />
      <Route exact path="/:id" component={PersonDetails} />
      <Redirect to="/" />
    </Switch>
  );
};
