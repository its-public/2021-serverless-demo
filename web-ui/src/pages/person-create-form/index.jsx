import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Toolbar from '@mui/material/Toolbar';
import AddIcon from '@mui/icons-material/Add';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import LoadingButton from '@mui/lab/LoadingButton';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { personsApi } from '@/api';
import { showSnackbar } from '@/store/snackbarSlice';
import { Formik, Form, FieldArray, getIn } from 'formik';
import { Helmet } from 'react-helmet-async';
import { format } from 'date-fns';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required('First name is required'),
  lastName: Yup.string().required('Last name is required'),
  dateOfBirth: Yup.string().required('Date of birth is required'),
  customFields: Yup.array().of(
    Yup.object().shape({
      key: Yup.string().required('Key is required'),
      value: Yup.string().required('value is required'),
    }),
  ),
});

const PersonCreateFormToolbar = () => {
  return (
    <Toolbar style={{ padding: 0 }}>
      <IconButton sx={{ mr: 3 }} component={Link} to="/">
        <ArrowBackIcon />
      </IconButton>
      <Typography sx={{ flex: '1 1 100%' }} variant="h6" component="div">
        New Person
      </Typography>
    </Toolbar>
  );
};

export const PersonCreateForm = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const dispatch = useDispatch();
  const history = useHistory();

  const createPerson = async (person) => {
    try {
      setIsLoading(true);
      await personsApi.createPerson(person);
      dispatch(showSnackbar({ message: 'Created successfully' }));
      setIsLoading(false);
      history.push('/');
    } catch (error) {
      setError(error);
      setIsLoading(false);
      dispatch(
        showSnackbar({ message: 'Failed to create. Try again', type: 'error' }),
      );
    }
  };

  return (
    <>
      <Helmet>
        <title>ITS | Create Person</title>
      </Helmet>
      <Box sx={{ px: 3, pb: 3 }}>
        <PersonCreateFormToolbar />
        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            dateOfBirth: '',
            customFields: [
              {
                key: '',
                value: '',
              },
            ],
          }}
          validationSchema={validationSchema}
          onSubmit={(values) => {
            createPerson(values);
          }}
        >
          {({
            values,
            touched,
            errors,
            handleChange,
            handleReset,
            setFieldValue,
            handleBlur,
          }) => (
            <Form noValidate autoComplete="off">
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <Stack spacing={2} sx={{ mt: 1 }}>
                  <Typography sx={{ flex: '1 1 100%', px: 1 }} component="div">
                    Bio
                  </Typography>
                  <Stack direction="row" spacing={1}>
                    <TextField
                      fullWidth
                      name="firstName"
                      label="First name"
                      required
                      value={values.firstName}
                      onChange={handleChange}
                      error={touched.firstName && Boolean(errors.firstName)}
                      helperText={touched.firstName && errors.firstName}
                      onBlur={handleBlur}
                    />
                    <TextField
                      fullWidth
                      name="lastName"
                      label="Last name"
                      required
                      value={values.lastName}
                      onChange={handleChange}
                      error={touched.lastName && Boolean(errors.lastName)}
                      helperText={touched.lastName && errors.lastName}
                      onBlur={handleBlur}
                    />
                  </Stack>
                  <Stack sx={{ maxWidth: '49.3%' }}>
                    <DesktopDatePicker
                      label="Date of birth"
                      name="dateOfBirth"
                      mask="____-__-__"
                      required
                      value={values.dateOfBirth}
                      onChange={(v) =>
                        setFieldValue('dateOfBirth', format(v, 'yyyy-MM-dd'))
                      }
                      renderInput={(params) => (
                        <TextField
                          id="dateOfBirth"
                          {...params}
                          error={
                            touched.dateOfBirth && Boolean(errors.dateOfBirth)
                          }
                          helperText={touched.dateOfBirth && errors.dateOfBirth}
                          onBlur={handleBlur}
                          required
                        />
                      )}
                      inputFormat="yyyy-MM-dd"
                    />
                  </Stack>
                </Stack>
                <Stack sx={{ mt: 2 }}>
                  <Typography
                    sx={{ flex: '1 1 100%', px: 1, mb: 1 }}
                    component="div"
                  >
                    Custom fields
                  </Typography>
                  <FieldArray name="customFields">
                    {({ push, remove }) => (
                      <div>
                        {values.customFields.map((f, index) => {
                          const key = `customFields[${index}].key`;
                          const touchedKey = getIn(touched, key);
                          const errorKey = getIn(errors, key);

                          const value = `customFields[${index}].value`;
                          const touchedValue = getIn(touched, value);
                          const errorValue = getIn(errors, value);

                          return (
                            <Stack
                              key={index}
                              direction="row"
                              spacing={1}
                              sx={{ my: 1 }}
                            >
                              <TextField
                                fullWidth
                                variant="outlined"
                                label="Key"
                                name={key}
                                value={f.key}
                                required
                                helperText={
                                  touchedKey && errorKey ? errorKey : ''
                                }
                                error={Boolean(touchedKey && errorKey)}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                              <TextField
                                fullWidth
                                variant="outlined"
                                label="Value"
                                name={value}
                                value={f.value}
                                required
                                helperText={
                                  touchedValue && errorValue ? errorValue : ''
                                }
                                error={Boolean(touchedValue && errorValue)}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                              <IconButton onClick={() => remove(index)}>
                                <CloseIcon />
                              </IconButton>
                            </Stack>
                          );
                        })}
                        <IconButton
                          color="primary"
                          onClick={() => push({ key: '', value: '' })}
                        >
                          <AddIcon />
                        </IconButton>
                      </div>
                    )}
                  </FieldArray>
                </Stack>
                {error && (
                  <Alert severity="error" sx={{ my: 2 }}>
                    {error.message}
                  </Alert>
                )}
                <Box
                  display="flex"
                  direction="row"
                  justifyContent="flex-end"
                  gap={2}
                >
                  <Button
                    type="button"
                    onClick={() => {
                      handleReset();
                      setError(null);
                    }}
                  >
                    Reset
                  </Button>
                  <LoadingButton
                    loading={isLoading}
                    type="submit"
                    color="primary"
                    variant="contained"
                  >
                    submit
                  </LoadingButton>
                </Box>
              </LocalizationProvider>
            </Form>
          )}
        </Formik>
      </Box>
    </>
  );
};
