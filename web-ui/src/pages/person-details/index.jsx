import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useParams } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import Alert from '@mui/material/Alert';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import Toolbar from '@mui/material/Toolbar';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import IconButton from '@mui/material/IconButton';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import CircularProgress from '@mui/material/CircularProgress';
import { selectPersonById } from '@/store/personsSlice';
import { showSnackbar } from '@/store/snackbarSlice';
import { personsApi } from '@/api';
import { Helmet } from 'react-helmet-async';

const PersonDetailsToolbar = ({ person }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const handleDeletePerson = async () => {
    try {
      await personsApi.deletePerson(person.personId);
      history.push('/');
      dispatch(showSnackbar({ message: 'Deleted successfully' }));
    } catch (error) {
      dispatch(
        showSnackbar({ message: 'Failed to delete. Try again', type: 'error' }),
      );
    }
  };

  return (
    <Toolbar style={{ padding: 0 }}>
      <IconButton sx={{ mr: 3 }} component={Link} to="/">
        <ArrowBackIcon />
      </IconButton>
      {person && (
        <>
          <Typography sx={{ flex: '1 1 100%' }} variant="h6" component="div">
            {person.firstName} {person.lastName}
          </Typography>
          <IconButton onClick={handleDeletePerson}>
            <DeleteOutlineIcon />
          </IconButton>
        </>
      )}
    </Toolbar>
  );
};

export const PersonDetails = () => {
  const { id: personId } = useParams();

  const [person, setPerson] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const cachedPerson = useSelector((state) =>
    selectPersonById(state, personId),
  );

  useEffect(() => {
    const getPerson = async () => {
      try {
        setIsLoading(true);
        const p = ((await personsApi.getPerson(personId)) || {}).data;
        setPerson(p);
      } catch (err) {
        setError(err);
        dispatch(
          showSnackbar({ message: 'Loading failed. Try again', type: 'error' }),
        );
      } finally {
        setIsLoading(false);
      }
    };

    if (cachedPerson) {
      setPerson(cachedPerson);
      return;
    }

    getPerson();
  }, []);

  if (error || isLoading) {
    return (
      <Box sx={{ px: 3, pb: 3 }}>
        <Helmet>
          <title>ITS | Person</title>
        </Helmet>
        <PersonDetailsToolbar />
        {isLoading && (
          <CircularProgress sx={{ mt: 28, display: 'block', mx: 'auto' }} />
        )}
        {error && (
          <Alert severity="error" sx={{ my: 2 }}>
            {error.message}
          </Alert>
        )}
      </Box>
    );
  }

  if (!person) {
    return (
      <Box sx={{ px: 3, pb: 3 }}>
        <Helmet>
          <title>ITS | Not Found</title>
        </Helmet>
        <PersonDetailsToolbar />
        <p>Not Found</p>
      </Box>
    );
  }

  return (
    <>
      <Helmet>
        <title>
          ITS | {person.firstName} {person.lastName}
        </title>
      </Helmet>
      <Box sx={{ px: 3, pb: 3 }}>
        <PersonDetailsToolbar person={person} />
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Stack spacing={2} sx={{ mt: 1 }}>
            <Typography sx={{ flex: '1 1 100%', px: 1 }} component="div">
              Bio
            </Typography>
            <Stack direction="row" spacing={1}>
              <TextField
                fullWidth
                label="First name"
                value={person.firstName}
                readOnly
              />
              <TextField
                fullWidth
                label="Last name"
                value={person.lastName}
                readOnly
              />
            </Stack>
            <Stack sx={{ maxWidth: '49.3%' }}>
              <DesktopDatePicker
                label="Date of birth"
                mask="____-__-__"
                readOnly
                onChange={() => {}}
                InputProps={{ readOnly: true }}
                value={person.dateOfBirth}
                renderInput={(params) => <TextField {...params} />}
                inputFormat="yyyy-MM-dd"
              />
            </Stack>
          </Stack>
          {person.customFields?.length && (
            <Stack sx={{ mt: 2 }}>
              <Typography
                sx={{ flex: '1 1 100%', px: 1, mb: 1 }}
                component="div"
              >
                Custom fields
              </Typography>

              {person.customFields?.map((f, index) => {
                return (
                  <Stack key={index} direction="row" spacing={1} sx={{ my: 1 }}>
                    <TextField
                      fullWidth
                      variant="outlined"
                      label="Key"
                      value={f.key}
                      readOnly
                    />
                    <TextField
                      fullWidth
                      variant="outlined"
                      label="Value"
                      value={f.value}
                      readOnly
                    />
                  </Stack>
                );
              })}
            </Stack>
          )}
        </LocalizationProvider>
      </Box>
    </>
  );
};
