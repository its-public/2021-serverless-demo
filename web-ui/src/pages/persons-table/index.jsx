import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import Menu from '@mui/material/Menu';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import InfoOutlineIcon from '@mui/icons-material/InfoOutlined';
import useMediaQuery from '@mui/material/useMediaQuery';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { personsApi } from '@/api';
import {
  setPersons,
  removePerson,
  selectAllPersons,
} from '@/store/personsSlice';
import { selectApiUrl } from '@/store/settingsSlice';
import { showSnackbar } from '@/store/snackbarSlice';
import { Helmet } from 'react-helmet-async';

const desktopColumns = [
  {
    field: 'firstName',
    sortable: true,
    flex: 1,
    headerName: 'First name',
    renderCell: (params) => <Box sx={{ pl: 2 }}>{params.row.firstName}</Box>,
  },
  {
    field: 'lastName',
    sortable: true,
    flex: 1,
    headerName: 'Last name',
  },
  {
    field: 'dateOfBirth',
    sortable: true,
    flex: 1,
    headerName: 'Date of birth',
    type: 'date',
    align: 'center',
  },
  {
    field: 'actions',
    headerName: 'Actions',
    type: 'actions',
    sortable: false,
    width: 90,
    align: 'center',
    renderCell: (params) => <PersonsTableRowMenu person={params.row} />,
  },
];

const mobileColumns = [
  {
    field: 'firstName',
    sortable: true,
    flex: 1,
    headerName: 'Full name',
    renderCell: (params) => (
      <Box sx={{ pl: 2 }}>
        {params.row.firstName} {params.row.lastName}
      </Box>
    ),
  },
  {
    field: 'actions',
    headerName: 'Actions',
    type: 'actions',
    sortable: false,
    width: 90,
    align: 'center',
    renderCell: (params) => <PersonsTableRowMenu person={params.row} />,
  },
];

const PersonsTableRowMenu = ({ person }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const dispatch = useDispatch();

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDeletePerson = async (personId) => {
    try {
      await personsApi.deletePerson(personId);
      dispatch(removePerson(personId));
      dispatch(showSnackbar({ message: 'Deleted successfully' }));
    } catch (error) {
      dispatch(
        showSnackbar({ message: 'Failed to delete. Try again', type: 'error' }),
      );
    }
  };

  return (
    <>
      <IconButton onClick={handleClick}>
        <MoreHorizIcon />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem component={Link} to={`/${person.personId}`}>
          <ListItemIcon sx={{ color: 'primary.main' }}>
            <InfoOutlineIcon />
          </ListItemIcon>
          <ListItemText>Details</ListItemText>
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleDeletePerson(person.personId);
          }}
        >
          <ListItemIcon sx={{ color: 'text.secondary' }}>
            <DeleteOutlineIcon />
          </ListItemIcon>
          <ListItemText>Delete</ListItemText>
        </MenuItem>
      </Menu>
    </>
  );
};

const PersonsTableToolbar = () => {
  return (
    <Toolbar>
      <Typography sx={{ flex: '1 1 100%' }} variant="h6" component="div">
        Persons
      </Typography>
      <Button
        variant="contained"
        component={Link}
        to="/new"
        disableElevation
        sx={{ borderRadius: '2px' }}
      >
        <AddIcon />
        Add
      </Button>
    </Toolbar>
  );
};

export const PersonsTable = () => {
  const apiUrl = useSelector(selectApiUrl);
  const persons = useSelector(selectAllPersons);
  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();
  const isMobile = useMediaQuery('(max-width:600px)');

  const getPersons = async () => {
    try {
      dispatch(setPersons([]));
      setIsLoading(true);
      const { persons } = ((await personsApi.getPersons()) || {}).data;
      dispatch(setPersons(persons));
    } catch (err) {
      dispatch(
        showSnackbar({ message: 'Loading failed. Try again', type: 'error' }),
      );
      dispatch(setPersons([]));
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getPersons();
  }, [apiUrl]);

  return (
    <>
      <Helmet>
        <title>ITS | Persons</title>
      </Helmet>
      <Paper sx={{ height: 695, width: '100%' }}>
        <DataGrid
          rows={persons}
          columns={isMobile ? mobileColumns : desktopColumns}
          pageSize={10}
          loading={isLoading}
          rowsPerPageOptions={[10]}
          components={{
            Toolbar: PersonsTableToolbar,
          }}
          getRowId={(r) => r.personId}
          disableColumnMenu
          disableColumnSelector
          disableDensitySelector
          disableSelectionOnClick
        />
      </Paper>
    </>
  );
};
