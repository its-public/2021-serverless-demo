import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { setSettings } from '@/store/settingsSlice';
import { API_URL } from '@/config';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  apiUrl: Yup.string().url('API URL must be a valid URL'),
});

export const AppSettingsForm = ({ onCancel, onSubmit }) => {
  const settings = useSelector((state) => state.settings);
  const dispatch = useDispatch();

  const form = useFormik({
    initialValues: {
      apiUrl: settings.apiUrl,
    },
    validationSchema,
    onSubmit: (values) => {
      dispatch(setSettings(values));
      onSubmit();
    },
  });

  const handleCancel = () => {
    form.handleReset();
    onCancel();
  };

  return (
    <form noValidate autoComplete="off" onSubmit={form.handleSubmit}>
      <Stack sx={{ pt: 1 }}>
        <TextField
          fullWidth
          name="apiUrl"
          label="API URL"
          value={form.values.apiUrl}
          onChange={form.handleChange}
          error={form.touched.apiUrl && Boolean(form.errors.apiUrl)}
          helperText={form.touched.apiUrl && form.errors.apiUrl}
          onBlur={() => {
            !form.values.apiUrl && form.setFieldValue('apiUrl', API_URL);
          }}
        />
      </Stack>
      <Stack
        sx={{
          mt: 2,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-end',
          gap: 1,
        }}
      >
        <Button color="primary" onClick={handleCancel}>
          Cancel
        </Button>
        <Button color="primary" variant="contained" type="submit" autoFocus>
          Save
        </Button>
      </Stack>
    </form>
  );
};
