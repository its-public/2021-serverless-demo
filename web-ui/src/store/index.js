import { configureStore } from '@reduxjs/toolkit';
import personsReducer from './personsSlice';
import snackbarReducer from './snackbarSlice';
import settingsReducer from './settingsSlice';

export const store = configureStore({
  reducer: {
    persons: personsReducer,
    snackbar: snackbarReducer,
    settings: settingsReducer,
  },
});
