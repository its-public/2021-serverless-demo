import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  persons: [],
};

export const personsSlice = createSlice({
  name: 'persons',
  initialState,
  reducers: {
    setPersons: (state, action) => {
      state.persons = action.payload;
    },
    removePerson: (state, action) => {
      state.persons = state.persons.filter(
        (p) => String(p.personId) !== String(action.payload),
      );
    },
    createPerson: (state, action) => {
      state.persons.push(action.payload);
    },
  },
});

export const { setPersons, removePerson, createPerson } = personsSlice.actions;

export const selectAllPersons = (state) => state.persons.persons;

export const selectPersonById = (state, id) =>
  state.persons.persons.find((p) => String(p.personId) === String(id));

export default personsSlice.reducer;
