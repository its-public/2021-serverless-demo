import { createSlice } from '@reduxjs/toolkit';
import { API_URL } from '@/config';
import { removeTrailingSlash } from '@/utils';

const initialState = {
  apiUrl: removeTrailingSlash(API_URL),
};

export const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    setSettings: (state, action) => {
      const apiUrl = removeTrailingSlash(action.payload?.apiUrl);
      Object.assign(state, action.payload, { apiUrl });
    },
  },
});

export const { setSettings } = settingsSlice.actions;

export const selectApiUrl = (state) => state.settings.apiUrl;

export default settingsSlice.reducer;
