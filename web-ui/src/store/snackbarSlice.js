import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  visible: false,
  message: '',
  type: '',
};

export const snackbarSlice = createSlice({
  name: 'snackbar',
  initialState,
  reducers: {
    showSnackbar: (state, action) => {
      state.visible = true;
      state.message = action.payload.message;
      state.type = action.payload.type;
    },
    hideSnackbar: (state) => {
      state.visible = false;
      state.message = '';
      state.type = '';
    },
    clearSnackbarInfo: (state) => {
      state.message = '';
      state.type = '';
    },
  },
});

export const { showSnackbar, hideSnackbar, clearSnackbarInfo } =
  snackbarSlice.actions;

export default snackbarSlice.reducer;
