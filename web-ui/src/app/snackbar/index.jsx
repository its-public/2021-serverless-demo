import { useDispatch, useSelector } from 'react-redux';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { hideSnackbar, clearSnackbarInfo } from '@/store/snackbarSlice';

export const AppSnackbar = () => {
  const snackbar = useSelector((state) => state.snackbar);
  const dispatch = useDispatch();

  const handleClose = () => {
    dispatch(hideSnackbar());
  };

  const handleExited = () => {
    dispatch(clearSnackbarInfo());
  };

  return (
    <Snackbar
      key={snackbar.message}
      open={snackbar.visible}
      autoHideDuration={3000}
      onClose={handleClose}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      TransitionProps={{ onExited: handleExited }}
    >
      <Alert
        onClose={handleClose}
        severity={snackbar.type || 'info'}
        sx={{ width: '100%' }}
      >
        {snackbar.message}!
      </Alert>
    </Snackbar>
  );
};
