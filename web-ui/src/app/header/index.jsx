import { useState } from 'react';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import logo from './logo.svg';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import SettingsIcon from '@mui/icons-material/Settings';
import { AppSettingsForm } from '@/features/app-settings-form';

const Settings = () => {
  const [isOpened, setIsOpened] = useState(false);

  const handleOpenClick = () => {
    setIsOpened(true);
  };

  const handleClose = () => {
    setIsOpened(false);
  };

  const handleSubmit = () => {
    setIsOpened(false);
  };

  return (
    <>
      <Dialog open={isOpened} onClose={handleClose} fullWidth maxWidth="xs">
        <DialogTitle>Settings</DialogTitle>
        <DialogContent>
          <AppSettingsForm onCancel={handleClose} onSubmit={handleSubmit} />
        </DialogContent>
      </Dialog>
      <IconButton sx={{ color: 'white' }} onClick={handleOpenClick}>
        <SettingsIcon />
      </IconButton>
    </>
  );
};

const Header = () => {
  return (
    <AppBar position="static">
      <Toolbar
        variant="dense"
        sx={{ display: 'flex', justifyContent: 'space-between', gap: 2 }}
      >
        <Box
          sx={{
            display: 'flex',
            gap: 2,
            alignItems: 'center',
          }}
        >
          <Box component={Link} to="/" sx={{ display: 'flex' }}>
            <Box component="img" src={logo} sx={{ height: 36 }} />
          </Box>
          <Typography
            sx={{
              textTransform: 'uppercase',
              color: 'white',
              fontWeight: 'bold',
              whiteSpace: 'nowrap',
            }}
            variant="h6"
            component="div"
          >
            Demo App
          </Typography>
        </Box>
        <Settings />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
