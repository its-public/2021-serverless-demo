import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import { PagesRouting } from '@/pages';
import { withProviders } from '@/providers';
import { AppSnackbar } from './snackbar';
import Header from './header';
import './index.css';

const App = () => {
  return (
    <Container
      className="app"
      maxWidth="sm"
      sx={{ height: '100%', px: { xs: 0 } }}
    >
      <Box sx={{ height: '100%' }}>
        <Header />
        <Paper
          sx={{
            minHeight: 695,
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
          }}
        >
          <PagesRouting />
        </Paper>
      </Box>
      <AppSnackbar />
    </Container>
  );
};

export default withProviders(App);
