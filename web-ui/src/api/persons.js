import { apiInstance } from './base';
import { store } from '@/store';

const RESOURCE_PATH = '/persons';

const getBaseUrl = () => {
  const { settings } = store.getState();

  return settings.apiUrl + RESOURCE_PATH;
};

export const getPersons = () => {
  return apiInstance.get(getBaseUrl());
};

export const createPerson = (person) => {
  return apiInstance.post(getBaseUrl(), person);
};

export const getPerson = (personId) => {
  return apiInstance.get(`${getBaseUrl()}/${personId}`);
};

export const deletePerson = (personId) => {
  return apiInstance.delete(`${getBaseUrl()}/${personId}`);
};
