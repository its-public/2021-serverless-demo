import { store } from '@/store';
import { Provider } from 'react-redux';

export const withRedux = (Component) => () => {
  return (
    <Provider store={store}>
      <Component />
    </Provider>
  );
};
