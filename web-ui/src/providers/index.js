import compose from 'compose-function';
import { withHelmet } from './with-helmet';
import { withRouter } from './with-router';
import { withMaterial } from './with-material';
import { withRedux } from './with-redux';

export const withProviders = compose(
  withMaterial,
  withRouter,
  withHelmet,
  withRedux,
);
