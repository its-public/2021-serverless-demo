import { HelmetProvider } from 'react-helmet-async';

export const withHelmet = (Component) => () => {
  return (
    <HelmetProvider>
      <Component />
    </HelmetProvider>
  );
};
