module "iam_policy_person_table_full_access" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-policy"
  version = "~> 4.3"

  name        = "${local.common_prefix}-person-table-full-access"
  path        = "/"
  description = "Allow writes and deletions to Persons table"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${module.dynamodb_persons_table.dynamodb_table_arn}"
    }
  ]
}
EOF
}
