locals {
  account_id = data.aws_caller_identity.current.account_id
}

module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  create_api_domain_name = false

  name          = "${var.project-name}-${var.env}-api"
  description   = "Serverless Persons API"
  protocol_type = "HTTP"

  cors_configuration = {
    allow_headers = ["content-type", "x-amz-date", "authorization", "x-api-key", "x-amz-security-token", "x-amz-user-agent"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }

  # Custom domain
  #  domain_name                 = "terraform-aws-modules.modules.tf"
  #  domain_name_certificate_arn = "arn:aws:acm:eu-west-1:052235179155:certificate/2b3a7ed9-05e1-4f9e-952b-27744ba06da6"

  # Access logs
  default_stage_access_log_destination_arn = module.lambda_person_handlers.lambda_cloudwatch_log_group_arn
  default_stage_access_log_format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"

  # Routes and integrations
  integrations = {
    "GET /persons" = {
      lambda_arn             = module.lambda_person_handlers.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 5000
    }

    "GET /persons/{personId}" = {
      lambda_arn             = module.lambda_person_handlers.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 5000
    }

    "POST /persons" = {
      lambda_arn             = module.lambda_person_handlers.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 5000
    }

    "DELETE /persons/{personId}" = {
      lambda_arn             = module.lambda_person_handlers.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 5000
    }

    "$default" = {
      lambda_arn = module.lambda_person_handlers.lambda_function_arn
    }
  }

  tags = local.common_tags
}