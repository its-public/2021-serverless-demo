#!/usr/bin/env bash

if [ $# -lt 2 ]; then
  echo "Usage:"
  echo "    $0 <gitlab-username> <gitlab-token> [other-tf-option]"
  echo "    To obtain token head to https://gitlab.com/-/profile/personal_access_tokens"
  exit 1
fi

GITLAB_USERNAME=$1
GITLAB_ACCESS_TOKEN=$2

# Initialize terraform with GitLab HTTP backend
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/30444537/terraform/state/2021-serverless-demo" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/30444537/terraform/state/2021-serverless-demo/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/30444537/terraform/state/2021-serverless-demo/lock" \
    -backend-config="username=$GITLAB_USERNAME" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5" \
    $3
