module "lambda_person_handlers" {
  source = "terraform-aws-modules/lambda/aws"

  cloudwatch_logs_retention_in_days = 14
  # Required to apply allowed_triggers
  # Ref. to Q4 in FAQ https://github.com/terraform-aws-modules/terraform-aws-lambda#faq
  publish       = true
  attach_policy = true

  function_name = "${var.project-name}-${var.env}-person-api-handlers"
  description   = "Handles Person API requests"
  handler       = "index.lambda_handler"
  runtime       = "python3.8"
  timeout       = 5

  source_path = "../lambda/person-api-handlers"

  allowed_triggers = {
    ApiGatewayAny = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.apigatewayv2_api_execution_arn}/*/*"
    }
  }
  policy                = module.iam_policy_person_table_full_access.arn
  tags                  = local.common_tags
  environment_variables = local.common_lambda_env
}
