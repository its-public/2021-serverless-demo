output "api_url" {
  value = module.api_gateway.default_apigatewayv2_stage_invoke_url
}

output "ddb_table_arn" {
  value = module.dynamodb_persons_table.dynamodb_table_arn
}

output "lambda_handler_arn" {
  value = module.lambda_person_handlers.lambda_function_arn
}
