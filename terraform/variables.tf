variable "env" {
  description = "Environment slug"
  type        = string
}

variable "project-name" {
  description = "Name of the project"
  type        = string
  default     = "sls-demo-kz"
}

variable "region" {
  description = "AWS region to use for deploy"
  type        = string
}