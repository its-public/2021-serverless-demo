# Provider Configuration
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "http" {}
}

provider "aws" {
  region = "eu-central-1"
}

data "aws_caller_identity" "current" {}

locals {
  common_prefix = "${var.project-name}-${var.env}"
  common_tags = {
    env          = var.env
    project-name = var.project-name
  }
  common_lambda_env = {
    PERSONS_TABLE_NAME = "${local.common_prefix}-persons"
  }
}