module "dynamodb_persons_table" {
  source = "terraform-aws-modules/dynamodb-table/aws"

  name     = "${local.common_prefix}-persons"
  hash_key = "personId"

  attributes = [
    {
      name = "personId"
      type = "S"
    }
  ]

  tags = local.common_tags
}