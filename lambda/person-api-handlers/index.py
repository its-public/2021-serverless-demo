import json
import os
import re
import uuid

import boto3
from boto3.dynamodb.conditions import Attr

TABLE_NAME = os.getenv("PERSONS_TABLE_NAME", None)
DDB = boto3.resource('dynamodb')
PERSONS_TABLE = DDB.Table(TABLE_NAME)
PERSON_BY_ID_REGEX = re.compile(r'/persons/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})')


def _flatten_custom_fields(person):
    result = dict(person)
    if 'customFields' in person:
        custom_fields = result.pop('customFields')
        for f in custom_fields:
            result[f['key']] = f['value']

    return result


def _unflatten_custom_fields(person):
    temp_person = dict(person)
    result = {
        'personId': temp_person.pop('personId'),
        'firstName': temp_person.pop('firstName'),
        'lastName': temp_person.pop('lastName'),
        'dateOfBirth': temp_person.pop('dateOfBirth')
    }

    # Check if there're custom fields left
    if len(temp_person) > 0:
        result['customFields'] = [{'key': k, 'value': v} for k, v in temp_person.items()]

    return result


def _get_all_persons():
    response = PERSONS_TABLE.scan()

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': json.dumps({
            'persons': [_unflatten_custom_fields(p) for p in response['Items']]
        })
    }


def _create_new_person(raw_person):
    # We use UUIDs as a person ID in order
    # to make key distribution in DynamoDB better
    person = dict(raw_person)
    person['personId'] = str(uuid.uuid4())
    if 'customFields' in person:
        person.pop('customFields')
        for f in raw_person['customFields']:
            person[f['key']] = f['value']

    PERSONS_TABLE.put_item(
        Item=person,
        ReturnValues='NONE',
        ConditionExpression=Attr('personId').not_exists()
    )

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': json.dumps(_unflatten_custom_fields(person))
    }


def _get_person(person_id):
    response = PERSONS_TABLE.get_item(
        Key={
            'personId': person_id
        },
        # Might be slower but ensures that new
        # Person will be returned immediately after creation
        ConsistentRead=True
    )

    # Operation did not return deleted item
    # It means there's no Person with given ID in the table
    if 'Item' not in response:
        return {
            'statusCode': 404
        }

    db_item = dict(response['Item'])
    person = _unflatten_custom_fields(db_item)

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': json.dumps(person)
    }


def _delete_person(person_id):
    response = PERSONS_TABLE.delete_item(
        Key={
            'personId': person_id
        },
        ReturnValues='ALL_OLD'
    )
    # Operation did not return deleted item
    # It means there's no Person with given ID in the table
    if 'Attributes' not in response:
        return {
            'statusCode': 404
        }

    return {
        "statusCode": 200
    }


def _cors(allowed_methods):
    return {
        'headers': {
            'Access-Control-Allow-Headers': 'content-type',
            'Access-Control-Allow-Methods': ','.join(allowed_methods).upper()
        },
        'statusCode': 200
    }


def lambda_handler(event, context):
    """
    API common entry point

    :param event:
    :param context:
    :return:
    """
    # Uncomment to debug input event
    # print(json.dumps(event))

    # Route request
    request_context = event.get('requestContext', None)
    if request_context is None:
        raise Exception('No request context found in event. Lambda supposed to be called by API Gateway.')

    method = request_context.get('httpMethod', None)
    if method == 'OPTIONS':
        return {
            'statusCode': 200
        }

    route_key = request_context['routeKey']

    if route_key == 'GET /persons':
        return _get_all_persons()
    elif route_key == 'GET /persons/{personId}':
        return _get_person(event['pathParameters']['personId'])
    elif route_key == 'POST /persons':
        person = json.loads(event['body'])
        return _create_new_person(person)
    elif route_key == 'DELETE /persons/{personId}':
        return _delete_person(event['pathParameters']['personId'])

    return {
        'statusCode': 404
    }
