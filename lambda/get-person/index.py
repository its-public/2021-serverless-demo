import json
import boto3
import os


TABLE_NAME = os.getenv("PERSONS_TABLE_NAME", None)
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(TABLE_NAME)


def lambda_handler(event, context):
    person_id = event['pathParameters']['personId']
    response = table.get_item(
        Key={
            'id': person_id
        },
        # Might be slower but ensures that new
        # Person will be returned immediately after creation
        ConsistentRead=True
    )
    # Operation did not return deleted item
    # It means there's no Person with given ID in the table
    if 'Item' not in response:
        return {
            'statusCode': 404
        }

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': json.dumps(response['Item'])
    }
