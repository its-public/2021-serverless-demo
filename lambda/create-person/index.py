import json
import os
import uuid

import boto3
from boto3.dynamodb.conditions import Attr

TABLE_NAME = os.getenv("PERSONS_TABLE_NAME", None)
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(TABLE_NAME)


def lambda_handler(event, context):
    person = json.loads(event['body'])
    # We use UUIDs as a person ID in order
    # to make key distribution in DynamoDB better
    person['id'] = str(uuid.uuid4())

    response = table.put_item(
        Item=person,
        ReturnValues='NONE',
        ConditionExpression=Attr('id').not_exists()
    )

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': json.dumps(person)
    }
