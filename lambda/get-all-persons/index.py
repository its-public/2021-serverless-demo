import json
import boto3
import os


TABLE_NAME = os.getenv("PERSONS_TABLE_NAME", None)
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(TABLE_NAME)


def lambda_handler(event, context):
    # Warning! Expensive operation
    # If all results will not fit into 1MB chunk then
    # DynamoDB will truncate them. In this case paging
    # must be implemented
    response = table.scan()

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': json.dumps({
            'persons': response['Items']
        })
    }