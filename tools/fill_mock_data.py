#!/usr/bin/env python

import http.client
import json
import logging
import os
from urllib.parse import urlparse

MOCKAROO_API_KEY = os.getenv('MOCKAROO_API_KEY', None)
API_BASE_URL = 'https://k1dakmyss0.execute-api.eu-central-1.amazonaws.com'

if MOCKAROO_API_KEY is None:
    print('MOCKAROO_API_KEY environment variable must be defined')
    exit(1)

LOGGER = logging.getLogger()


def _get_mock_data():
    LOGGER.info('Fetching mock data from Mockaroo')

    conn = http.client.HTTPSConnection('api.mockaroo.com')
    with open('mock-schema.json', 'r', encoding='utf8') as ms:
        payload = json.load(ms)

    headers = {
      'x-api-key': MOCKAROO_API_KEY,
      'Content-Type': 'application/json',
    }

    conn.request('POST', '/api/generate.json?count=10', json.dumps(payload), headers)
    res = conn.getresponse()
    data = res.read()
    json_data = json.loads(data)

    LOGGER.info('%s items fetched', len(json_data))
    return json_data


def _upload_data(data: dict) -> None:
    LOGGER.info('Uploading info about persons')
    parse_result = urlparse(API_BASE_URL)
    if parse_result.scheme == 'http':
        conn = http.client.HTTPConnection(parse_result.netloc)
    else:
        conn = http.client.HTTPSConnection(parse_result.netloc)

    headers = {
        'Content-Type': 'application/json'
    }

    for person in data:
        conn.request('POST', '/persons', json.dumps(person), headers)
        r = conn.getresponse()
        r.read()

    LOGGER.info('%s persons have been created', len(data))


def main():
    mock_data = _get_mock_data()
    _upload_data(mock_data)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
